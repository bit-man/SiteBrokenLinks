package javalabs.brokenlinks.mock;


import javalabs.brokenlinks.ILinkFetcher;
import javalabs.brokenlinks.WebPage;
import javalabs.brokenlinks.WebPageRetrievalErrorException;

import java.net.URL;
import java.util.Map;

public class LinkFetcherMock
    implements ILinkFetcher
{

    private Map<URL, WebPage> pages;

    public LinkFetcherMock(Map<URL,WebPage> pages)
    {
        this.pages = pages;
    }

    @Override
    public WebPage getPage(URL url)
            throws WebPageRetrievalErrorException
    {
        if ( ! pages.containsKey(url) ){
            throw new WebPageRetrievalErrorException("MOCK: page not found");
        }

        return pages.get(url);
    }
}
