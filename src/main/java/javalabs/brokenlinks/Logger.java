package javalabs.brokenlinks;


public class Logger
{
    private static AppContext ctx = AppContext.getInstance();
    private static java.util.logging.Logger l = java.util.logging.Logger.getAnonymousLogger();

    public static void logDebug(String msg)
    {
        l.finest(msg);
    }

    static {
        l.setLevel(ctx.getLogLevel());
    }

    public static void logError(String msg)
    {
        l.severe(msg);
    }

    public static void log(Throwable e)
    {
        l.fine( e.getMessage() );
        for( StackTraceElement elem : e.getStackTrace() ) {
            l.fine(elem.toString());
        }
    }

    public static void logInfo(String msg)
    {
        l.info(msg);
    }
}
