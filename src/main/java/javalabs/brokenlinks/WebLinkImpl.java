package javalabs.brokenlinks;

import java.net.URL;

public class WebLinkImpl
        implements WebLink
{
    private final URL url;

    public WebLinkImpl(URL url)
    {
        this.url = url;
    }

    @Override
    public URL getURL()
    {
        return url;
    }
}
