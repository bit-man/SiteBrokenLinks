package javalabs.brokenlinks.mock;

import javalabs.brokenlinks.WebLink;

import java.net.URL;

public class WebLinkMock
        implements WebLink
{
    private final URL url;

    public WebLinkMock(URL url)
    {
        this.url = url;
    }

    public URL getURL()
    {
        return url;
    }
}
