package javalabs.brokenlinks.driver;

import javalabs.brokenlinks.WebPage;
import javalabs.brokenlinks.WebPageRetrievalErrorException;

import java.net.URL;

public interface IWebDriver
{
    WebPage getPage(URL url)
            throws WebPageRetrievalErrorException;
}
