package javalabs.brokenlinks.test;

import javalabs.brokenlinks.LinkFetcher;
import javalabs.brokenlinks.WebPage;
import javalabs.brokenlinks.WebPageRetrievalErrorException;
import javalabs.brokenlinks.mock.WebDriverMock;
import org.junit.Test;

import java.net.MalformedURLException;
import java.net.URL;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertTrue;
import static junit.framework.TestCase.assertNotNull;

public class LinkFetcherTest

{
    @Test
    public void testConstructorNotNull() {
        assertNotNull( new LinkFetcher( new WebDriverMock() ) );
    }


    @Test
    public void testObtainWebPage()
            throws MalformedURLException, WebPageRetrievalErrorException
    {
        final LinkFetcher linkFetcher = new LinkFetcher(new WebDriverMock());
        final WebPage webPage = linkFetcher.getPage(new URL("http://url1"));
        assertNotNull(webPage);
    }

    @Test
    public void testObtainWebPageWithNoLinks()
            throws MalformedURLException, WebPageRetrievalErrorException
    {
        final LinkFetcher linkFetcher = new LinkFetcher(new WebDriverMock());
        final WebPage webPage = linkFetcher.getPage(new URL("http://url1"));
        assertTrue(webPage.getLinks().isEmpty());
    }

    @Test(expected = WebPageRetrievalErrorException.class)
    public void testErrorOnWebPageRetrieval()
            throws MalformedURLException, WebPageRetrievalErrorException
    {
        final LinkFetcher linkFetcher = new LinkFetcher(new WebDriverMock());
        linkFetcher.getPage(new URL("http://pageNotFound"));

    }

    @Test
    public void testObtainWebPageWithOneLink()
            throws MalformedURLException, WebPageRetrievalErrorException
    {
        final LinkFetcher linkFetcher = new LinkFetcher(new WebDriverMock());
        final WebPage webPage = linkFetcher.getPage(new URL("http://url2"));
        assertEquals(1, webPage.getLinks().size());
        assertEquals(new URL("http://url2/link1"), webPage.getLinks().iterator().next().getURL());
    }


}
