package javalabs.brokenlinks;

import java.util.Collection;

public class WebPageImpl
    implements WebPage
{
    private final Collection<WebLink> links;

    public WebPageImpl(Collection<WebLink> links)
    {
        this.links = links;
    }

    @Override
    public Collection<WebLink> getLinks()
    {
        return links;
    }
}
