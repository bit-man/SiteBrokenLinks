package javalabs.brokenlinks.test;

import javalabs.brokenlinks.LinkChecker;
import javalabs.brokenlinks.WebLink;
import javalabs.brokenlinks.WebPage;
import javalabs.brokenlinks.mock.LinkFetcherMock;
import javalabs.brokenlinks.mock.WebLinkMock;
import javalabs.brokenlinks.mock.WebPageMock;
import org.junit.Test;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;

import static org.junit.Assert.*;


public class LinkCheckerTest
{
    @Test
    public void testConstructorNotNull()
            throws MalformedURLException
    {
        assertNotNull(new LinkChecker(new URL("http://localhost"), new LinkFetcherMock(new HashMap<URL, WebPage>())));
    }

    @Test
    public void testNoPagesCheckEndsInError()
            throws MalformedURLException
    {
        final LinkChecker linkChecker = new LinkChecker(new URL("http://localhost"), new LinkFetcherMock(new HashMap<URL, WebPage>()));
        final LinkChecker.Response response = linkChecker.check();
        assertNotNull(response);
        assertFalse(response.isSuccess());
        assertEquals(1, response.getBrokenLinks().size());
        assertEquals( new URL("http://localhost"), response.getBrokenLinks().iterator().next() );
        assertTrue(response.getUnverifiedLinks().isEmpty());
    }

    @Test
    public void testOnePageWithNoLinksEndsOk()
            throws MalformedURLException
    {
        final HashMap<URL, WebPage> links = new HashMap<>();
        links.put(new URL("http://localhost"), new WebPageMock( new ArrayList<WebLink>() ));
        final LinkChecker linkChecker = new LinkChecker(new URL("http://localhost"), new LinkFetcherMock(links));
        final LinkChecker.Response response = linkChecker.check();
        assertNotNull(response);
        assertTrue(response.isSuccess());
        assertTrue(response.getBrokenLinks().isEmpty());
        assertTrue(response.getUnverifiedLinks().isEmpty());
    }

    @Test
    public void testOnePageWithOneBrokenLinksEndInError()
            throws MalformedURLException
    {
        final HashMap<URL, WebPage> links = new HashMap<>();
        final ArrayList<WebLink> localhostLinks = new ArrayList<>();
        final URL nonExistentPage = new URL("http://localhost/page1");
        localhostLinks.add( new WebLinkMock(nonExistentPage) );
        links.put(new URL("http://localhost"), new WebPageMock(localhostLinks));
        final LinkChecker linkChecker = new LinkChecker(new URL("http://localhost"), new LinkFetcherMock(links));
        final LinkChecker.Response response = linkChecker.check();
        assertNotNull(response);
        assertFalse(response.isSuccess());
        assertEquals(1, response.getBrokenLinks().size());
        assertEquals(nonExistentPage, response.getBrokenLinks().iterator().next());
        assertTrue(response.getUnverifiedLinks().isEmpty());
    }

    @Test
    public void testOnePageWithExternalLinkEndsOkButAddsToNotVerifiedPages()
            throws MalformedURLException
    {

        final HashMap<URL, WebPage> links = new HashMap<>();
        final ArrayList<WebLink> webLinks = new ArrayList<>();
        final URL externalSite = new URL("http://external.site");
        webLinks.add( new WebLinkMock(externalSite) );
        links.put(new URL("http://localhost"), new WebPageMock(webLinks));
        final LinkChecker linkChecker = new LinkChecker(new URL("http://localhost"), new LinkFetcherMock(links));
        final LinkChecker.Response response = linkChecker.check();
        assertNotNull(response);
        assertTrue(response.isSuccess());
        assertTrue(response.getBrokenLinks().isEmpty());
        assertEquals(1, response.getUnverifiedLinks().size());
        assertEquals(externalSite, response.getUnverifiedLinks().iterator().next());
    }


}
