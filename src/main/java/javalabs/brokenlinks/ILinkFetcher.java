package javalabs.brokenlinks;

import java.net.URL;

public interface ILinkFetcher
{

    WebPage getPage(URL url)
            throws WebPageRetrievalErrorException;
}
