package javalabs.brokenlinks;

import javalabs.brokenlinks.driver.IWebDriver;

import java.net.URL;

public class LinkFetcher
        implements ILinkFetcher
{
    private final IWebDriver driver;

    public LinkFetcher(IWebDriver driver)
    {
        this.driver = driver;
    }

    public WebPage getPage(URL url)
            throws WebPageRetrievalErrorException
    {
        return driver.getPage(url);
    }
}
