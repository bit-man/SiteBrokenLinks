package javalabs.brokenlinks.mock;

import javalabs.brokenlinks.WebLink;
import javalabs.brokenlinks.WebPage;

import java.util.ArrayList;
import java.util.Collection;

public class WebPageMock
        implements WebPage
{

    public WebPageMock(ArrayList<WebLink> links)
    {
        this.links = links;
    }

    private ArrayList<WebLink> links;

    public Collection<WebLink> getLinks()
    {
        return links;
    }
}
