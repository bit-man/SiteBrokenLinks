package javalabs.brokenlinks;

import javalabs.brokenlinks.driver.SeleniumDriver;

import java.net.MalformedURLException;
import java.net.URL;

public class Main
{
    public static void main(String[] args)
            throws MalformedURLException
    {
        // ToDo add parametrized driver selection
        final LinkChecker.Response response = new LinkChecker(new URL(args[0]), new LinkFetcher(new SeleniumDriver()))
                .check();

        if (response.isSuccess()) {
            System.out.println("All links OK");
        } else {
            System.out.println("Broken links : " + response.getBrokenLinks().size());
            for( URL link : response.getBrokenLinks() ) {
                System.out.println("\t" + link);
            }
        }


        System.out.println("Unverified links : " + response.getUnverifiedLinks().size());
        for( URL link : response.getUnverifiedLinks() ) {
            System.out.println("\t" + link);
        }

    }
}
