package javalabs.brokenlinks;

public class WebPageRetrievalErrorException
        extends Throwable
{
        public WebPageRetrievalErrorException(String msg)
        {
                super(msg);
        }
}
