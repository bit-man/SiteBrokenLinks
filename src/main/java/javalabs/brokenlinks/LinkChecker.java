package javalabs.brokenlinks;

import java.net.URL;
import java.util.*;

public class LinkChecker
{
    private final ILinkFetcher linkFetcher;
    private final URL site;

    public LinkChecker(URL site, ILinkFetcher linkFetcher)
    {
        this.site = site;
        this.linkFetcher = linkFetcher;
    }

    public Response check()
    {

        Collection<URL> unverifiedLinks = new LinkedList<>();
        Collection<URL> brokenLinks = new LinkedList<>();

        // ToDo test for not visit already visited links
        Set<URL> visitedLinks = new HashSet<>();
        Set<URL> pendingLinks = new HashSet<>();
        pendingLinks.add(site);

        // ToDo refactor to parallel verision (threads + streams)
        while (!pendingLinks.isEmpty())
        {
            URL currentPageURL = null;
            final Iterator<URL> iterator = pendingLinks.iterator();
            try
            {
                currentPageURL = iterator.next();
                iterator.remove();

                if (!  visitedLinks.contains(currentPageURL) )
                {
                    processLink(unverifiedLinks, visitedLinks, pendingLinks, currentPageURL);
                }
            } catch (WebPageRetrievalErrorException e)
            {
                Logger.logError("Broken link detected (" + currentPageURL + ") : " + e.getMessage());
                brokenLinks.add(currentPageURL);
            }
        }

        return new Response(brokenLinks, unverifiedLinks);
    }

    private void processLink(Collection<URL> unverifiedLinks, Set<URL> visitedLinks, Set<URL> pendingLinks,
                             URL currentPageURL)
            throws WebPageRetrievalErrorException
    {
        visitedLinks.add(currentPageURL);

        if (currentPageURL.getHost().equals(site.getHost()))
        {
            obtainLinks(pendingLinks, currentPageURL);
        } else
        {
            // External links
            // ToDo verify it's reachable but don't scan for links
            Logger.logInfo("External link detected (" + currentPageURL + ")");
            unverifiedLinks.add(currentPageURL);
        }
    }

    private void obtainLinks(Set<URL> pendingLinks, URL page)
            throws WebPageRetrievalErrorException
    {
        Logger.logInfo("Retrieving page " + page);
        for (WebLink link : linkFetcher.getPage(page).getLinks())
        {
            pendingLinks.add(link.getURL());
        }
    }

    // ToDo when reporting a broken link also show in which page(s) it is pointed from
    public class Response
    {
        private Collection<URL> brokenLinks;
        private Collection<URL> unverifiedLinks;

        public Response(Collection<URL> brokenLinks, Collection<URL> unverifiedLinks)
        {
            this.brokenLinks = brokenLinks;
            this.unverifiedLinks = unverifiedLinks;
        }

        public Collection<URL> getBrokenLinks()
        {
            return brokenLinks;
        }

        public boolean isSuccess()
        {
            return brokenLinks.size() == 0;
        }

        public Collection<URL> getUnverifiedLinks()
        {
            return this.unverifiedLinks;
        }
    }
}
