package javalabs.brokenlinks;

import java.net.URL;

public interface WebLink
{
    URL getURL();
}
