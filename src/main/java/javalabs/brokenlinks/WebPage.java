package javalabs.brokenlinks;

import java.util.Collection;

public interface WebPage
{
    Collection<WebLink> getLinks();
}
