package javalabs.brokenlinks;

import java.util.logging.*;

public class AppContext
{
    private static AppContext singleton = null;
    private Level logLevel;

    private AppContext(){}

    public static AppContext getInstance()
    {
        if (singleton == null) {
            singleton = new AppContext();
            singleton.logLevel = Level.parse( System.getProperty("sbl.logLevel", "INFO") );
        }

        return singleton;
    }

    public Level getLogLevel()
    {
        return logLevel;
    }
}
