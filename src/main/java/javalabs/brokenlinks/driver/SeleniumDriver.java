package javalabs.brokenlinks.driver;

import javalabs.brokenlinks.*;
import org.openqa.selenium.By;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

// ToDo testing ....
public class SeleniumDriver
        implements IWebDriver
{
    // ToDo let's pass driver to use
    private WebDriver driver = new FirefoxDriver();


    @Override
    public WebPage getPage(URL url)
            throws WebPageRetrievalErrorException
    {
        driver.get(url.toString());
        List<WebElement> aTag = null;
        try
        {
            // Using explicit wait allows timeout catching and thus reporting the caller for this condition
            // instead of returning an empty list in case of failure (implicit waiting)
            aTag = new WebDriverWait(driver, 10)
                    .until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.tagName("a")));
        }
        catch(TimeoutException e) {
            throw new WebPageRetrievalErrorException("Timeout");
        }

        // FIXME this is true only for sites I've using to test this tool
        // (pass site ad-hoc code detection ??)
        // use proxy : http://www.supermind.org/blog/968/howto-collect-webdriver-http-request-and-response-headers

        if (driver.getTitle().contains("404")) {
            // Page not found !
            // HTTP error codes won't be added to API
            // (https://code.google.com/p/selenium/issues/detail?id=141)
            throw new WebPageRetrievalErrorException("Page not found");
        }

        List<WebLink> links = new ArrayList<>(aTag.size());
        for (WebElement elem : aTag)
        {
            try
            {
                final String href = elem.getAttribute("href");
                try
                {
                    // ToDo what means that an anchor doesn't have href attribute ?
                    if (href != null)
                    {
                        links.add(new WebLinkImpl(buildFullyQualifiedURL(href, url)));
                    }
                } catch (MalformedURLException e)
                {
                    Logger.logError("Unrecognized link : " + href);
                    Logger.log(e);
                }
            } catch (Throwable e)
            {
                // Avoids stopping on page retrieval errors
                Logger.log(e);
            }
        }
        return new WebPageImpl(links);
    }

    private URL buildFullyQualifiedURL(String href, URL baseURL)
            throws MalformedURLException
    {
        // URL format : <scheme>://<authority><path>?<query>#<fragment>
        if (href.startsWith("/"))
        {
            // Has no host/port
            Logger.logDebug("absolute URl, no host/port : " + href);
            return new URL(baseURL.getProtocol() + "://" + baseURL.getAuthority() + href);
        }

        final String[] protocolSplit = href.split(":");
        if (protocolSplit.length > 1)
        {
            // There's protocol => is a FQ URL already
            if (!protocolSplit[0].equals("javascript"))
            {
                Logger.logDebug("Fully qualified URL already : " + href);
                return new URL(href);
            } else
            {
                throw new MalformedURLException("Cannot handle javascript links");
            }
        }

        // Else is a relative URL

        if (href.startsWith("#"))
        {
            throw new MalformedURLException("Cannot handle javascript links");
        }

        Logger.logDebug("Relative URL : " + href);
        return new URL(baseURL.toString() + "/" + href);
    }

}
