package javalabs.brokenlinks.mock;

import javalabs.brokenlinks.driver.IWebDriver;
import javalabs.brokenlinks.WebLink;
import javalabs.brokenlinks.WebPage;
import javalabs.brokenlinks.WebPageRetrievalErrorException;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

public class WebDriverMock
        implements IWebDriver
{
    public WebPage getPage(URL url)
            throws WebPageRetrievalErrorException
    {
        try
        {
            if ( url.equals( new URL("http://url1") ))
                return new WebPageMock( new ArrayList<WebLink>() );


            if ( url.equals( new URL("http://url2") )) {
                final ArrayList<WebLink> links = new ArrayList<>();
                links.add( new WebLinkMock( new URL("http://url2/link1") ) );
                return new WebPageMock(links);
            }

        } catch (MalformedURLException e)
        {
            // Don't care
        }

        throw  new WebPageRetrievalErrorException("MOCK: page not found");
    }


}
